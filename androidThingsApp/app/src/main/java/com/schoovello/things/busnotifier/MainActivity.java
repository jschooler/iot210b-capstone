package com.schoovello.things.busnotifier;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.android.things.contrib.driver.sensehat.LedMatrix;
import com.google.android.things.contrib.driver.sensehat.SenseHat;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends Activity {

	private static final boolean DEBUG = false;

	private static final String CLOUD_FUNCTION_URL = "https://us-central1-bus-notifier-d7022.cloudfunctions.net/arrivals";

	private static final String STOP_ID = "1_538"; // 3rd and Columbia NW-bound
	private static final String ROUTE_ID = "1_102615"; // E-Line

	private static final long MONITOR_DELAY_MS = 30_000; // query service every 30 sec
	private static final long DRAW_DELAY_MS = 5_000; // draw to SenseHat every 5 sec

	private long[] mArrivalTimes = new long[0];

	private MonitorTask mInProgressTask;
	private boolean mResumed;
	private Handler mHandler;

	private LedMatrix mLedMatrix;
	private Bitmap mBitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mHandler = new Handler(Looper.getMainLooper());

		mBitmap = Bitmap.createBitmap(8, 8, Bitmap.Config.ARGB_8888);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (mBitmap != null) {
			mBitmap.recycle();
			mBitmap = null;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		try {
			mLedMatrix = SenseHat.openDisplay();
		} catch (IOException e) {
			e.printStackTrace();
		}

		mResumed = true;
		mHandler.post(mMonitorRunnable);
		mHandler.post(mDrawRunnable);
	}

	@Override
	protected void onPause() {
		super.onPause();

		mResumed = false;

		// cancel in-progress task and release
		if (mInProgressTask != null) {
			mInProgressTask.cancel(false);
			mInProgressTask = null;
		}

		// cancel all scheduled Runnables
		mHandler.removeCallbacksAndMessages(null);

		// release LED matrix
		if (mLedMatrix != null) {
			try {
				mLedMatrix.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private final Runnable mMonitorRunnable = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(this);

			mInProgressTask = new MonitorTask();
			mInProgressTask.execute();

			mHandler.postDelayed(this, MONITOR_DELAY_MS);
		}
	};

	private class MonitorTask extends AsyncTask<Void, Void, long[]> {
		@Override
		protected long[] doInBackground(Void... voids) {
			if (DEBUG) {
				long now = System.currentTimeMillis();
				return new long[] { now + 3 * 60 * 1000, now + 22 * 60 * 1000, now + 2100000 };
			}

			long[] arrivals = new long[0];
			try {
				OkHttpClient httpClient = new OkHttpClient.Builder().build();

				// build the URL
				Uri.Builder urlBuilder = Uri.parse(CLOUD_FUNCTION_URL).buildUpon();
				urlBuilder.appendQueryParameter("stopId", STOP_ID);
				urlBuilder.appendQueryParameter("routeId", ROUTE_ID);

				// perform the request
				Request request = new Request.Builder().url(urlBuilder.toString()).build();
				Response response = httpClient.newCall(request).execute();

				// parse the response
				JSONObject json = new JSONObject(response.body().string());
				JSONArray arrivalsArray = json.getJSONArray("arrivals");
				int arrivalsCount = arrivalsArray.length();
				if (arrivalsCount > 0) {
					arrivals = new long[arrivalsCount];
					for (int i = 0; i < arrivalsCount; i++) {
						arrivals[i] = arrivalsArray.getLong(i);
					}
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}

			return arrivals;
		}

		@Override
		protected void onPostExecute(long[] result) {
			// store the arrival timestamps
			mArrivalTimes = result;

			// clear the task
			mInProgressTask = null;
		}
	}

	private final Runnable mDrawRunnable = new Runnable() {
		@Override
		public void run() {
			mHandler.removeCallbacks(this);

			try {
				// draw the arrival times
				drawArrivals();
			} catch (IOException e) {
				e.printStackTrace();
			}

			// re-schedule self
			mHandler.postDelayed(this, DRAW_DELAY_MS);
		}
	};

	private void drawArrivals() throws IOException {
		if (mLedMatrix == null || mBitmap == null) {
			return;
		}

		// if there are none (or there was a network error) draw an error screen (all red)
		if (mArrivalTimes.length == 0) {
			mLedMatrix.draw(0xffff0000);
			return;
		}

		// clear the canvas by drawing black to every pixel
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				mBitmap.setPixel(x, y, 0xff000000);
			}
		}

		// draw the first two arrivals to the bitmap
		if (mArrivalTimes.length > 0) {
			drawArrival(mBitmap, 0, mArrivalTimes[0]);
		}
		if (mArrivalTimes.length > 1) {
			drawArrival(mBitmap, 4, mArrivalTimes[1]);
		}

		// draw the bitmap to the LED array
		mLedMatrix.draw(mBitmap);
	}

	private static void drawArrival(Bitmap bitmap, int yOffset, long arrivalMs) {
		// compute the number of minutes until this timestamp
		final long etaMs = arrivalMs - System.currentTimeMillis();
		final long arrivalSec = etaMs / 1000;
		final int arrivalMin = (int) (arrivalSec / 60);

		// pick the color depending on how soon it will arrive
		// 5+: green
		// 3-4: yellow
		// 0-2: red
		final int color = arrivalMin >= 5 ? 0xff00ff00 : arrivalMin >= 3 ? 0xffffff00 : 0xffff0000;

		Log.d("ME", "arrival minutes: " + arrivalMin);

		// if it's arriving in less than 1 minute, round up to a whole minute so that at least
		// one pixel is filled
		final int dotCount = Math.max(1, arrivalMin);

		// fill pixels
		for (int x = 0; x < 8; x++) {
			for (int y = 0 + yOffset; y < 4 + yOffset; y++) {
				boolean on = (y - yOffset) * 8 + x < dotCount;
				if (on) {
					bitmap.setPixel(x, y, color);
				}
			}
		}
	}
}
