# IOT 210B: Internet of Things: protocols & Networks
## Jonathan Schooler - Capstone project

The purpose of this project is to create a visual representation of real-time Metro bus arrival information. I used the OneBusAway public API to get the data from Sound Transit.

This project focuses on Google's Android Things IoT platform. It consists of two applications: a cloud function hosted on Firebase, and an Android application deployed to a Raspberry Pi running Android Things OS.

## Setup

1. Install Android Things onto a Raspberry Pi as instructed by https://developer.android.com/things/hardware/raspberrypi.html.
1. Create a Firebase project at https://firebase.google.com and install the Firebase command-line tools. Upgrade the project to a paid account so that external network access is allowed from Firebase Cloud Functions.
1. Deploy the cloud functions from the firebaseCloudFunctions directory.
1. Install the SenseHat (with LED matrix) on the Rasperry Pi.
1. Build the Android app from the androidThingsApp directory and install it on the Raspberry Pi. Modify hard-coded bus stop and route IDs as desired.
