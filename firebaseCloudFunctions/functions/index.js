'use strict';

const functions = require('firebase-functions');
const request = require('request');
const oneBusAway = require('./apis/oba');

// import the OneBusAway API key from environment vars
const obaKey = functions.config().one_bus_away.api_key;

function sendError(response, errorObject) {
	console.log(errorObject);
	response.status(500).json(errorObject);
}

// main function "arrivals"
exports.arrivals = functions.https.onRequest((request, response) => {
	var stopId = request.query.stopId;
	var routeId = request.query.routeId;

	// check params
	if (!stopId || !routeId) {
		response.status(400).send('Missing stopId or routeId');
		return;
	}

	// query OneBusAway for the stopId
	var oba = oneBusAway(obaKey);
	oba.arrivalsAndDeparturesForStop(stopId, function(err, obaResponse) {
		if (err) {
			sendError(response, err);
			return;
		}

		try {
			var currentTime = obaResponse.currentTime;
			console.log('currentTime', currentTime);

			var arrivalsAndDepartures = obaResponse.data.entry.arrivalsAndDepartures;

			// get predicted timestamps
			var arrivals = [];
			for (var i = 0; i < arrivalsAndDepartures.length; i++) {
				var entry = arrivalsAndDepartures[i];

				// we only care about live data (predicted times) in the future
				if (entry.routeId === routeId && entry.predicted
							&& entry.predictedArrivalTime > currentTime) {
					arrivals.push(entry.predictedArrivalTime);
				}
			}

			// sort by timestamp
			arrivals.sort(function(a, b) {
				return a - b;
			});

			// create response object
			var responseObj = {
				currentTime: currentTime,
				arrivals: arrivals
			};

			// send response
			response.status(200).json(responseObj);
		} catch (error) {
			sendError(response, error);
		}
	});

})
