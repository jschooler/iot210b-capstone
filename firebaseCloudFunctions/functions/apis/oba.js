// oba.js

const URL_BASE = 'http://api.pugetsound.onebusaway.org/api/where/';

var Request = require('request');

function defaultOptions(apiKey) {
	return {
		method: 'GET',
		qs: {
			key: apiKey
		}
	}
}

var OneBusAway = function(apiKey) {
	return {
		arrivalsAndDeparturesForStop: function(stopId, callback) {
			// build HTTP request
			var options = defaultOptions(apiKey);
			options.url = URL_BASE + 'arrivals-and-departures-for-stop/' + stopId + '.json';

			// perform HTTP request
			Request.get(options, function(err, resp, body) {
				if (!callback) {
					console.log('No callback given');
					return;
				}

				if (err) {
					callback(err, null);
				} else {
					try {
						// parse HTTP response
						var parsed = JSON.parse(body);

						// execute callback
						callback(null, parsed);
					} catch (e) {
						callback(e, null);
					}
				}
			});
		}
	}
}

module.exports = OneBusAway;
